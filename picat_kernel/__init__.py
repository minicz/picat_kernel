"""An Picat kernel for Jupyter"""

__version__ = '0.1'

from .kernel import PicatKernel
